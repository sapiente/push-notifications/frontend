# Requirements

- npm >= 6.4.1
- openssl
- docker 

# Configuration

- `.env.local` file for env variables (https://facebook.github.io/create-react-app/docs/adding-custom-environment-variables#what-other-env-files-can-be-used)
    - REACT_APP_VAPID_PUBLIC_KEY
- `configs/docker/variables.env` file for docker container
    - API_URL
- `configs/ssl/localhost.cnf` file for SSL certificate

# Installation

## Local

- `npm ci`

## Docker

- `npm ci`
- `npm run build`
- `./create_cert.sh` (inside *configs/ssl* folder)
- `docker-compose build --no-cache web` (inside *configs/docker* folder)

# Running

## Local

- `npm run start`

## Docker

- `docker-compose up -d web` (inside *configs/docker* folder)