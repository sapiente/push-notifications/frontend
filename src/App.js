import React from 'react';
import './App.css';
import {isSupported} from "./pushNotifications";
import PushNotificationsButton from "./PushNotificationsButton";

function App() {
    return (
        <div className="App">
            <header className="App-header">
                <img src="/images/logo.svg" className="App-logo" alt="logo"/>
                <p>Push notifications PoC</p>
            </header>
            {isSupported() && <PushNotificationsButton/>}
        </div>
    );
}

export default App;
