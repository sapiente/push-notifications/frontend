const publicVapidKey = process.env.REACT_APP_VAPID_PUBLIC_KEY;

function urlBase64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/-/g, '+')
        .replace(/_/g, '/');

    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);

    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

async function registerServiceWorker() {
    const req = await navigator.serviceWorker.register('/service-worker-app.js');
    const subscription = await req.pushManager.getSubscription();

    if (subscription !== null) {
        await updateSubscription(subscription);
    }
}

export async function unsubscribeUser() {
    const req = await navigator.serviceWorker.getRegistration();
    const subscription = await req.pushManager.getSubscription();

    if(!subscription) {
        console.error('Unable to unregister push notification.');
        return;
    }

    try {
        await subscription.unsubscribe();
        await deleteSubscription(subscription);
    } catch (e) {
        console.error('Failed to unsubscribe push notification.');
    }
}

export async function subscribeUser() {
    const reg = await navigator.serviceWorker.getRegistration();

    try {
        const subscription = await reg.pushManager.subscribe({
            userVisibleOnly: true,
            applicationServerKey: urlBase64ToUint8Array(publicVapidKey)
        });

        await updateSubscription(subscription);
    } catch (err) {
        if (Notification.permission === 'denied') {
            console.warn('Permission for notifications was denied');
        } else {
            console.error('Unable to subscribe to push', err.message);
        }
    }
}

function updateSubscription(subscription) {
    return fetch(`/api/push-notifications/subscriptions`, {
        method: 'PUT',
        body: JSON.stringify(subscription),
        headers: {
            'Content-Type': 'application/json',
        },
    });
}

function deleteSubscription(subscription) {
    return fetch(`/api/push-notifications/subscriptions`, {
        method: 'DELETE',
        body: JSON.stringify(subscription),
        headers: {
            'Content-Type': 'application/json',
        },
    });
}

export function isSupported() {
    return 'PushManager' in window && 'Notification' in window && Notification.permission !== 'denied' ;
}

export async function isSubscribed() {
    const req = await navigator.serviceWorker.getRegistration();
    const subscription = await req.pushManager.getSubscription();

    return !!subscription;
}

export function register() {
    if ('Notification' in window && navigator.serviceWorker) {
        Notification.requestPermission(function (status) {
            console.log('Notification permission status:', status);

            registerServiceWorker().catch((err) => console.error('Service Worker registration failed: ', err));
        });
    }
}